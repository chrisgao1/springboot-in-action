#!/bin/bash

export APP_BASE=$(cd `dirname $0`;cd ../;pwd)
export APP_TARGET="${APP_BASE}/target"
export APP_TARGET_BACKUP=${APP_TARGET}/backup
export APP_NAME="springboot-in-action.*[.]jar"

if [ ! -d $APP_TARGET ]; then
  echo "${APP_TARGET} directory does not exist"
  mkdir -p $APP_TARGET
  exit 0
fi

app=`ls -l ${APP_TARGET} | grep ${APP_NAME} | awk '{print $NF}'`

if [ -z "$app" ]; then
  echo "${APP_NAME} file does not exist"
  exit 2
fi

if [ ! -d $APP_TARGET_BACKUP ]; then
  mkdir -p $APP_TARGET_BACKUP
fi

appname=`echo $app | sed 's/[.]jar$//1'`$(date "+%Y%m%d%H%M").jar
cp ${APP_TARGET}/${app} ${APP_TARGET_BACKUP}/${appname}
echo "${app} is backed up -- ${appname}"

