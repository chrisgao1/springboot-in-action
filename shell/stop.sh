#!/bin/bash

####stop####

pid=`ps -ef | grep springboot-in-action | grep java | grep -v "grep" | awk '{print $2}'`

if [ -z "$pid" ]; then
  echo "No springboot-in-action app is running"
else
  echo "springboot-in-action app ${pid} is running"
  kill -9 $pid
  echo "springboot-in-action app ${pid} shut down"
fi

