#!/bin/bash

####start####
export APP_BASE=$(cd `dirname $0`;cd ../;pwd)
export APP_TARGET=${APP_BASE}/target
export APP_TARGET_BACKUP=${APP_TARGET}/backup
export APP_NAME="springboot-in-action*.jar"
export APP_LOGS=${APP_BASE}/logs

if [ ! -d "$APP_LOGS" ]; then
  mkdir -p $APP_LOGS
fi

if [ ! -d "$APP_TARGET_BACKUP" ]; then
  mkdir -p $APP_TARGET_BACKUP
fi

if [ ! -f ${APP_TARGET}/${APP_NAME} ]; then
  echo "springboot-in-action app not exists"
  exit 2
fi

nohup java -jar ${APP_TARGET}/${APP_NAME} >> ${APP_LOGS}/springboot-in-action-$(date "+%Y%m%d").log 2>&1 &

pid=`ps -ef | grep springboot-in-action | grep java | grep -v "grep" | awk '{print $2}'`

if [ -z "$pid" ]; then
  echo "springboot-in-action app failed to launch"
else
  echo "springboot-in-action app ${pid} is running"
fi

