package org.schenker.gc.springboot;

import org.junit.Assert;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.schenker.gc.springboot.dao.FacadeDao;
import org.schenker.gc.springboot.service.impl.FacadeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * Lang: Java
 * ClassName: ApplicationTest
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 8/6/2022 11:29 PM
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ApplicationTest {

    @InjectMocks
    private FacadeServiceImpl facadeService; // InjectMocks不能是接口, @Mock标记的类的只有在InjectMocks里引用才会是mock实现，否则还是原方法

    @Mock
    private FacadeDao facadeDao;

    @Test
    public void testMockObject(){
        Mockito.doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                String input = (String)args[0];
                return input.toLowerCase();
            }
        }).when(facadeDao).toUpper(Mockito.anyString());
        String actual1 = facadeDao.toUpper("Juventus FC");
        System.out.println(actual1);
        String actual = facadeService.getUpper("Juventus FC");
        Assert.assertEquals("juventus fc", actual);
    }
}
