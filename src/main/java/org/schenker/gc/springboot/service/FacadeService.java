package org.schenker.gc.springboot.service;

/**
 * Lang: Java
 * ClassName: FacadeService
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 8/7/2022 12:05 AM
 */
public interface FacadeService {
    String getUpper(String input);
}
