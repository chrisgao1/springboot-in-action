package org.schenker.gc.springboot.service.impl;

import org.schenker.gc.springboot.dao.FacadeDao;
import org.schenker.gc.springboot.service.FacadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Lang: Java
 * ClassName: FacadeServiceImpl
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 8/7/2022 12:05 AM
 */
@Service
public class FacadeServiceImpl implements FacadeService {

    @Autowired
    private FacadeDao facadeDao;

    @Override
    public String getUpper(String input) {
        return facadeDao.toUpper(input);
    }
}
