package org.schenker.gc.springboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Lang: Java
 * ClassName: HomeController
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 8/6/2022 11:07 PM
 */
@RestController
@RequestMapping("/home")
public class HomeController {
    @GetMapping({"", "index"})
    public Map index(){
        return new HashMap(){{
            put("name", "Stephen Curry");
            put("number", 30);
        }};
    }
}
