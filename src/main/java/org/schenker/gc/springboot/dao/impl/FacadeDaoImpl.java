package org.schenker.gc.springboot.dao.impl;

import org.schenker.gc.springboot.dao.FacadeDao;
import org.springframework.stereotype.Repository;

/**
 * Lang: Java
 * ClassName: FacadeDaoImpl
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 8/6/2022 11:33 PM
 */
@Repository
public class FacadeDaoImpl implements FacadeDao {
    @Override
    public String toUpper(String input) {
        return input.toUpperCase();
    }
}
