package org.schenker.gc.springboot.dao;

/**
 * Lang: Java
 * ClassName: FacadeDao
 * Description: Whatever it takes
 *
 * @author CHRISGAO
 * @version 1.0.0
 * @since 8/6/2022 11:33 PM
 */
public interface FacadeDao {
    String toUpper(String input);
}
