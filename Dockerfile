ARG VERSION=8

FROM openjdk:${VERSION}-alpine

WORKDIR /home

COPY target/*.jar /home

EXPOSE 8010

ENTRYPOINT java -jar *.jar
