<?php
    $local_base = "/var/java/springboot_in_action/repository";
    $local_repos = $local_base."/springboot-in-action";
    $token = $_SERVER['HTTP_X_GITLAB_TOKEN'];
    echo $token;

    $input = file_get_contents("php://input");
    if (empty($input)) {
        die('send fail');
    }
    $data = json_decode($input, true);

    echo $data['ref'];
    echo $data['total_commits_count'];

    if (!empty($token) && $data['ref'] == "refs/heads/master" && $data['total_commits_count'] > 0) {
        $res = shell_exec("cd {$local_repos} && git pull origin master");
        $res_log.= $data['user_name'].' '.date('Y-m-d H:i:s').' '.$data['repository']['name'].' '.$data['ref'].' '.$data['total_commits_count'].' commit'.PHP_EOL;
        $res_log.= $res.PHP_EOL;
        file_put_contents($local_base."/web.log", $res_log, FILE_APPEND);
    }
?>
